import { Injectable } from '@angular/core';
import { Todo } from '../interfaces/todo'
import { environment } from 'src/environments/environment'
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { catchError } from 'rxjs/operators'
import { throwError as observableThrowError } from 'rxjs'


const API_URL = environment.apiUrl

@Injectable({
  providedIn: 'root'

})
export class TodoService {
  todoTitle: string = '';
  idForTodo: number = 4;
  beforeEditCache: string = '';
  filter: string = 'all';
  anyRemainingModel: boolean = true
  todos: Todo[] = [];

  constructor(private http: HttpClient) {
    this.todos = this.getTodos()
  }

  getTodos(): Todo[] {
    console.info("todo.services excute getTodos")
    try {
      this.http.get(API_URL)
        .pipe(catchError(this.errorHandler))
        .subscribe((response: any) => {
          this.todos = response
          console.debug("this.todos", this.todos)
        })
      return this.todos
    } catch (e) {
      console.error(`todo.services getTodos Error ${e}`)
    }
  }

  errorHandler(error: HttpErrorResponse) {
    console.info("todo.services excute errorHandler")
    return observableThrowError(error.message || 'Something went wrong!')
  }

  addTodo(todoTitle: string): void {
    console.info("todo.services excute addTodo")
    console.debug("todoTitle", todoTitle)
    try {
      if (todoTitle.trim().length === 0) {
        return
      }
      this.http.post(API_URL, {
        title: todoTitle,
        completed: false
      }).subscribe((response: any) => {
        this.todos.push({
          id: response.id,
          title: todoTitle,
          completed: false,
          editing: false
        })
      })
      this.idForTodo++
    } catch (e) {
      console.error(`todo.services addTodo Error ${e}`)
    }
  }

  editTodo(todo: Todo): void {
    console.info("todo.services excute editTodo")
    console.debug("todo", todo)
    try {
      this.beforeEditCache = todo.title
      todo.editing = true;
    } catch (e) {
      console.error(`todo.services editTodo Error ${e}`)
    }
  }

  doneEdit(todo: Todo): void {
    console.info("todo.services excute doneEdit")
    console.debug("todo", todo)
    try {
      if (todo.title.trim().length === 0) {
        todo.title = this.beforeEditCache;
      }
      todo.editing = false;
      this.http.patch(API_URL + '/' + todo.id, {
        title: todo.title,
        completed: todo.completed
      }).subscribe((response: any) => {

      })
    } catch (e) {
      console.error(`todo.services editTodo Error ${e}`)
    }
  }

  cancelEdit(todo: Todo): void {
    console.info("todo.services excute cancelEdit")
    console.debug("todo", todo)
    console.debug("dadad", this.beforeEditCache)
    try {
      todo.title = this.beforeEditCache;
      todo.editing = false;
    } catch (e) {
      console.error(`todo.services cancelEdit Error ${e}`)
    }
  }

  deleteTodo(id: number): void {
    console.log("todo.services excute deleteTodo")
    try {
      this.http.delete(API_URL + '/' + id)
        .subscribe((response: any) => {
          this.todos = this.todos.filter(todo => todo.id !== id)
        })
    } catch (e) {
      console.error(`todo.services deleteTodo Error ${e}`)

    }
  }

  remaining(): number {
    console.log("todo.services excute remaining")
    try {
      return this.todos.filter(todo => !todo.completed).length
    } catch (e) {
      console.error(`todo.services remaining Error ${e}`)
    }
  }

  atLeastOneCompleted(): boolean {
    console.log("todo.services excute atLeastOneCompleted")
    try {
      return this.todos.filter(todo => todo.completed).length > 0;
    } catch (e) {
      console.error(`todo.services atLeastOneCompleted Error ${e}`)
    }
  }

  clearCompleted(): void {
    console.log("todo.services excute clearCompleted")
    try {
      this.todos = this.todos.filter(todo => !todo.completed)
      // const completed = this.todos.filter(todo=> todo.completed).map(todo =>todo.id)
      // this.http.request("delete", API_URL + '/todoDeleteCompleted', {
      //   body: {
      //     todos: completed
      //   }
      // }).subscribe((response: any) => {
      //   this.todos = this.todos.filter(todo => !todo.completed)
      // })
    } catch (e) {
      console.error(`todo.services clearCompleted Error ${e}`)
    }
  }

  checkAllTodos(): void {
    console.log("todo.services excute checkAllTodos")
    try {
      this.todos.forEach(todo => todo.completed = (<HTMLInputElement>event.target).checked)
      const checkedTodo = (<HTMLInputElement>event.target).checked;
      // this.http.patch(API_URL +'/', {
      //   completed: checkedTodo
      // }).subscribe((response: any) => {
      //   this.todos.forEach(todo => todo.completed = checkedTodo)
      //   this.anyRemainingModel = this.anyRemainingModel
      // })  
    } catch (e) {
      console.error(`todo.services checkAllTodos Error ${e}`)
    }
  }

  anyRemaining(): boolean {
    return this.remaining() !== 0
  }

  todosFiltered(): Todo[] {
    console.log("todo.services excute todosFiltered")
    try {
      if (this.filter === 'all') {
        return this.todos
      } else if (this.filter == 'active') {
        return this.todos.filter(todo => !todo.completed)
      } else if (this.filter === 'completed') {
        return this.todos.filter(todo => todo.completed)
      }
      return this.todos
    } catch (e) {
      console.error(`todo.services todosFiltered Error ${e}`)
    }
  }

}
