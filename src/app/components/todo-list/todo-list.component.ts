import { animate, animation, style, transition, trigger } from '@angular/animations';
import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { TodoService } from 'src/app/services/todo.service';
import {Todo} from '../../interfaces/todo'

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
  providers: [TodoService],
  animations: [
    trigger('fade', [
      transition(':enter', [
        style({opacity: 0, transform: 'translateY(30px'}),
        animate(1000, style({opacity:1 , transform: 'translateY(0px'}))
      ]),
      transition(':leave', [
        animate(1000, style({opacity:0, transform: 'translateY(30px)'}))
      ])
    ])
  ]
})
export class TodoListComponent implements OnInit {
  // todos: Todo[];
  todoTitle: string;
  // idForTodo: number;
  // beforeEditCache: string;
  // filter: string;
  // private todoService: TodoService;

  constructor(public todoService: TodoService) {
    // this.todoService =  todoService;
   }

  ngOnInit(): void {  
    // console.info("todo-list.component excute ngOnInit")
    // this.filter= 'all'
    // this.beforeEditCache = ''
    // this.idForTodo = 4;
    this.todoTitle = '';
    // this.todos= [
    //   {
    //     'id': 1,
    //     'title': 'Finish Angular Screencast',
    //     'completed': false,
    //     'editing': false,
    //   },
    //   {
    //     'id': 2,
    //     'title': 'Take over world',
    //     'completed': false,
    //     'editing': false,
    //   },
    //   {
    //     'id': 3,
    //     'title': 'One more thing',
    //     'completed': false,
    //     'editing': false,
    //   },
    // ]
  }

  addTodo():void {
   console.info("todo-list.component excute addTodo") 
   console.debug("todoTitle", this.todoTitle)
   try {
     if(this.todoTitle.trim().length === 0) {
       return
     }
     this.todoService.addTodo(this.todoTitle)
    //  this.todos.push({
    //    id: this. idForTodo,
    //    title: this.todoTitle,
    //    completed: false,
    //    editing: false
    //  })

     this.todoTitle= '';
    //  this.idForTodo++ 
   } catch(e) {
     console.error(`todo-list.component addTodo Error ${e}`)
   }
  }

  // editTodo(todo: Todo): void {
  //   console.info("todo-list.component excute editTodo") 
  //   console.debug("todo", todo) 
  //   try {
  //     this.beforeEditCache = todo.title
  //     todo.editing = true;
  //   } catch(e) {
  //     console.error(`todo-list.component editTodo Error ${e}`)
  //   }
  // }

  // doneEdit(todo: Todo): void {
  //   console.info("todo-list.component excute doneEdit")
  //   console.debug("todo", todo) 
  //   try {
  //     if(todo.title.trim().length === 0) {
  //       todo.title = this.beforeEditCache;
  //     }
  //     todo.editing = false;
  //   } catch(e) {
  //     console.error(`todo-list.component editTodo Error ${e}`)
  //   }
  // }

  // cancelEdit(todo: Todo): void {
  //   console.info("todo-list.component excute cancelEdit")
  //   console.debug("todo", todo) 
  //   console.debug("dadad", this.beforeEditCache)
  //   try {
  //     todo.title = this.beforeEditCache;
  //     todo.editing = false;
  //   } catch(e) {
  //     console.error(`todo-list.component cancelEdit Error ${e}`)
  //   }
  // }

  // deleteTodo(id: number): void {
  //   console.log("todo-list.component excute deleteTodo") 
  //   try {
  //     this.todos = this.todos.filter(todo => todo.id !== id)
  //   } catch(e) {
  //     console.error(`todo-list.component deleteTodo Error ${e}`)

  //   }
  // }

  // remaining(): number {
  //   console.log("todo-list.component excute remaining") 
  //   try {
  //     return this.todos.filter(todo => !todo.completed).length
  //   }catch(e) {
  //     console.error(`todo-list.component remaining Error ${e}`)
  //   }
  // }

  // atLeastOneCompleted(): boolean {
  //   console.log("todo-list.component excute atLeastOneCompleted") 
  //   try {
  //     return this.todos.filter(todo => todo.completed).length > 0;
  //   }catch(e) {
  //     console.error(`todo-list.component atLeastOneCompleted Error ${e}`)
  //   }
  // }

  // clearCompleted(): void {
  //   console.log("todo-list.component excute clearCompleted") 
  //   try {
  //     this.todos = this.todos.filter(todo => !todo.completed)
  //   }catch(e) {
  //     console.error(`todo-list.component clearCompleted Error ${e}`)
  //   }
  // }

  // checkAllTodos(): void {
  //   console.log("todo-list.component excute checkAllTodos") 
  //   try {
  //     console.log("xx", this.todos)
  //     this.todos.forEach(todo => todo.completed = (<HTMLInputElement>event.target).checked)
  //     console.log("yy", this.todos)
  //   }catch(e) {
  //     console.error(`todo-list.component checkAllTodos Error ${e}`)
  //   }
  // }

  // todosFiltered(): Todo[] {
  //   console.log("todo-list.component excute todosFiltered") 
  //   try {
  //    if(this.filter === 'all') {
  //      return this.todos
  //    } else if (this.filter == 'active') {
  //      return this.todos.filter(todo => !todo.completed)
  //    } else if (this.filter === 'completed') {
  //      return this.todos.filter(todo => todo.completed)
  //    }
  //    return this.todos
  //   }catch(e) {
  //     console.error(`todo-list.component todosFiltered Error ${e}`)
  //   }
  // }

}

